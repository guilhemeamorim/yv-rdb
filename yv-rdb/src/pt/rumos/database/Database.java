package pt.rumos.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Database {
    private static final String URL = "jdbc:mysql://localhost:3306/rdb-marques";
    private static final String USERNAME = "root";
    private static final String PASSWORD = "root";

    private static Connection conn;
    private static Statement stmt;

    private Database() {}

    private static void init() throws SQLException {
        if (conn == null) {
            conn = DriverManager.getConnection(URL, USERNAME, PASSWORD);
            stmt = conn.createStatement();
        }
    }

    public static ResultSet executeQuery(String sql) {
        try {
            init();
            return stmt.executeQuery(sql);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public static void execute(String sql) {
        try {
            init();
            stmt.execute(sql);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }
}