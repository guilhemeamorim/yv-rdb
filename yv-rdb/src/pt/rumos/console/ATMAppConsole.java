package pt.rumos.console;

import java.util.Scanner;

public class ATMAppConsole {
    //Todo o código de interação com o utilizador para ATM
    
    public void run() {
        @SuppressWarnings("resource")
        Scanner scanner = new Scanner(System.in);
        ConsoleUtils utils = new ConsoleUtils();
        utils.header("ATM Module");
        int option = 0;
        do {
            System.out.println("Select an Option:");
            System.out.println("0: Exit");
            System.out.println("1: Withdraw");
            System.out.println("2: Deposit");
            System.out.println("3: Transfer");
            option = scanner.nextInt();
            switch (option) {
                case 1:
                    System.out.println("Withdrawing...");
                    break;
                case 2:
                    System.out.println("Depositing...");
                    break;
                case 3:
                    System.out.println("Transfering...");
                    break;
            }

        } while (option != 0);
        System.out.println("Thanks for using the ATM Module");
    }

}
