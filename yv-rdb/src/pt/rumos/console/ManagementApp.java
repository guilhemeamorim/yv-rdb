package pt.rumos.console;

import java.time.LocalDate;
import java.util.Scanner;

import pt.rumos.exception.ResourceNotFoundException;
import pt.rumos.model.Account;
import pt.rumos.model.Client;
import pt.rumos.repository.AccountRepository;
import pt.rumos.repository.AccountRepositorySQLImpl;
import pt.rumos.repository.ClientRepository;
import pt.rumos.repository.ClientRepositorySQLImpl;
import pt.rumos.service.AccountService;
import pt.rumos.service.AccountServiceImpl;
import pt.rumos.service.ClientService;
import pt.rumos.service.ClientServiceImpl;

public class ManagementApp {
    // Todo o código de interação com o utilizador para a gestão de:
    // Clientes, Contas e Cartões
    private Scanner scanner = new Scanner(System.in);


    private ClientRepository clientRepo = new ClientRepositorySQLImpl();
    private AccountRepository accountRepo = new AccountRepositorySQLImpl();
    private ClientService clientService = new ClientServiceImpl(clientRepo);
    private AccountService accountService = new AccountServiceImpl(accountRepo);

    public void run() {
        ConsoleUtils utils = new ConsoleUtils();
        int option = 0;
        utils.header("Management Module");
        do {
            showOptionsMenu();
            option = scanner.nextInt();
            switch (option) {
                case 1:
                    clientManagement();
                    break;
                case 2:
                    accountManagement();
                    break;
                case 3:
                    System.out.println("Managing cards...");
                    break;
            }

        } while (option != 0);
        System.out.println("Thanks for using the Management Module");
    }

    private void accountManagement() {
        int option = 0;
        do {
            System.out.println("Select an Option:");
            System.out.println("0: Exit");
            System.out.println("1: Add Account");
            System.out.println("2: List All Accounts");
            System.out.println("3: Show Account By Account Number");
            System.out.println("4: Delete Account By ID");
            option = scanner.nextInt();
            switch (option) {
                case 1:
                    Account account = createNewAccount();
                    accountService.save(account);
                    break;
                case 2:
                    for (Account acc : accountService.getAll()) {
                        System.out.println(acc);
                    }
                    break;
                case 3:
                    System.out.println("Please insert account number");
                    Long id = scanner.nextLong();
                    Account existingAccount = accountService.getById(id);
                    System.out.println(existingAccount);
                    break;
                case 4:
                    break;
            }

        } while (option != 0);


    }

    private Account createNewAccount() {
        Account account = new Account();

        System.out.println("Please insert account number");
        Integer accountNumber = scanner.nextInt();
        account.setAccountNumber(accountNumber);

        System.out.println("Please insert owner ID");
        for (Client c : clientService.getAll()) {
            System.out.println(c);
        }

        Client owner = clientService.getById(scanner.nextLong());
        account.setOwner(owner);

        System.out.println("Please insert initial balance");
        Double balance = scanner.nextDouble();
        account.setBalance(balance);
        return account;
    }

    private void clientManagement() {
        int option = 0;
        do {
            System.out.println("Select an Option:");
            System.out.println("0: Exit");
            System.out.println("1: Add Client");
            System.out.println("2: List All Clients");
            System.out.println("3: Show Client By NIF");
            System.out.println("4: Delete Client By ID");
            option = scanner.nextInt();
            switch (option) {
                case 1:
                    Client client = createNewClient();
                    clientService.save(client);
                    break;
                case 2:
                    for (Client c : clientService.getAll()) {
                        System.out.println(c);
                    }
                    break;
                case 3:
                    System.out.println("Please insert client NIF");
                    String nif = scanner.next();
                    try {
                        Client existingClient = clientService.getByNif(nif);
                        System.out.println(existingClient);

                    } catch (ResourceNotFoundException e) {
                        System.err.println(e.getMessage());
                    }
                    break;
                case 4:
                    System.out.println("Please insert client ID");
                    Long id = scanner.nextLong();
                    clientService.deleteById(id);
                    break;
            }

        } while (option != 0);

    }

    private Client createNewClient() {
        Client client = new Client();

        System.out.println("Please insert client Name");
        String name = scanner.next();
        client.setName(name);

        System.out.println("Please insert client NIF");
        String nif = scanner.next();
        client.setNif(nif);

        System.out.println("Please insert client Password");
        String password = scanner.next();
        client.setPassword(password);

        // System.out.println("Please insert client birth Day");
        // Integer day = scanner.nextInt();

        // System.out.println("Please insert client birth Month");
        // Integer month = scanner.nextInt();

        // System.out.println("Please insert client birth Year");
        // Integer year = scanner.nextInt();

        // LocalDate dob = LocalDate.of(year, month, day);
        // client.setDob(dob);

        System.out.println("Please insert client Date of Birth YYYY-MM-DD");
        String dob = scanner.next();
        client.setDob(LocalDate.parse(dob));
        return client;
    }

    private void showOptionsMenu() {
        System.out.println("Select an Option:");
        System.out.println("0: Exit");
        System.out.println("1: Clients");
        System.out.println("2: Accounts");
        System.out.println("3: Cards");
    }
}
