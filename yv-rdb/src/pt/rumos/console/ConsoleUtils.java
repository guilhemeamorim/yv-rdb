package pt.rumos.console;

public class ConsoleUtils {

    public void header(String moduleName) {
        System.out.println("*******************");
        System.out.println("**Welcome to the " + moduleName + "**");
        System.out.println("*******************");
    }
    
}
