//package pt.rumos.jfx.service;
//
//import java.util.List;
//
//import javafx.collections.FXCollections;
//import javafx.collections.ObservableList;
//import pt.rumos.model.Client;
//import pt.rumos.repository.ClientRepositoryArrayImpl;
//import pt.rumos.service.ClientService;
//import pt.rumos.service.ClientServiceImpl;
//
//public class AppServiceImpl implements AppService {
//
//    private ClientService clientService;
//
//    public AppServiceImpl() {
//        this.clientService = new ClientServiceImpl(new ClientRepositoryArrayImpl());
//    }
//
//    @Override
//    public ObservableList<Client> getAll() throws Exception {
//        List<Client> clients = clientService.getAll();
//        return FXCollections.observableArrayList(clients);
//    }
//
//    @Override
//    public Client save(Client client) throws Exception {
//        return clientService.save(client);
//    }
//
//    @Override
//    public void deleteById(String id) throws Exception {
//        clientService.deleteById(Long.valueOf(id));
//    }
//
//}
