//package pt.rumos.jfx.view;
//
//import java.net.URL;
//import java.util.ResourceBundle;
//import java.util.stream.Collectors;
//
//import javafx.collections.FXCollections;
//import javafx.collections.ObservableList;
//import javafx.concurrent.Task;
//import javafx.event.ActionEvent;
//import javafx.fxml.FXML;
//import javafx.fxml.Initializable;
//import javafx.scene.control.Alert;
//import javafx.scene.control.Alert.AlertType;
//import javafx.scene.control.Button;
//import javafx.scene.control.DatePicker;
//import javafx.scene.control.Label;
//import javafx.scene.control.TableColumn;
//import javafx.scene.control.TableRow;
//import javafx.scene.control.TableView;
//import javafx.scene.control.TextField;
//import javafx.scene.control.cell.PropertyValueFactory;
//import javafx.scene.layout.VBox;
//import pt.rumos.jfx.service.AppService;
//import pt.rumos.jfx.service.AppServiceImpl;
//import pt.rumos.model.Client;
//
//public class AppView implements Initializable {
//
//    private AppService service = new AppServiceImpl();
//    private ObservableList<Client> clientTableViewContents;
//    private ObservableList<Client> data;
//
//    @FXML
//    private VBox dataContainer;
//    @FXML
//    private TableView<Client> clientTableView;
//    @FXML
//    private Label idLabel;
//    @FXML
//    private TextField idField;
//    @FXML
//    private Label nameLabel;
//    @FXML
//    private TextField nameField;
//    @FXML
//    private Label nifLabel;
//    @FXML
//    private TextField nifField;
//    @FXML
//    private Label dobLabel;
//    @FXML
//    private DatePicker dobField;
//    @FXML
//    private Label phoneLabel;
//    @FXML
//    private TextField phoneField;
//    @FXML
//    private Label mobileLabel;
//    @FXML
//    private TextField mobileField;
//    @FXML
//    private Label emailLabel;
//    @FXML
//    private TextField emailField;
//    @FXML
//    private Label occupationLabel;
//    @FXML
//    private TextField occupationField;
//    @FXML
//    private Label passwordLabel;
//    @FXML
//    private TextField passwordField;
//    @FXML
//    private Button saveClientButton;
//    @FXML
//    private Button removeClientButton;
//    @FXML
//    private Button clearClientForm;
//
//    public AppView() {
//        clientTableView = new TableView<Client>();
//        clientTableViewContents = FXCollections.observableArrayList();
//        data = FXCollections.observableArrayList(clientTableViewContents);
//    }
//
//    @Override
//    public void initialize(URL location, ResourceBundle resources) {
//        try {
//            initClientTable();
//            initClientTableListener();
//            updateClientListView();
//        } catch (Exception e) {
//            showError(e);
//        }
//    }
//
//    public void addClient(ActionEvent event) {
//        Client client = new Client();
//        if (idField.getText() == null || idField.getText().isEmpty()) {
//            client.setId(null);
//        } else {
//            client.setId(Long.valueOf(idField.getText()));
//        }
//        client.setName(nameField.getText());
//        client.setNif(nifField.getText());
//        client.setDob(dobField.getValue());
//        client.setTelephone(phoneField.getText());
//        client.setMobile(mobileField.getText());
//        client.setEmail(emailField.getText());
//        client.setOccupation(occupationField.getText());
//        client.setPassword(passwordField.getText());
//        try {
//            Client savedClient = service.save(client);
//            fillAllFields(savedClient);
//            updateClientListView();
//        } catch (Exception e) {
//            showError(e);
//            clearAllFields();
//        }
//    }
//
//    public void removeClient(ActionEvent event) {
//        if (idField.getText() == null || idField.getText().isEmpty())
//            return;
//        try {
//            service.deleteById(idField.getText());
//            clearAllFields();
//            updateClientListView();
//        } catch (Exception e) {
//            showError(e);
//        }
//    }
//
//    public void clearForm(ActionEvent event) {
//        clearAllFields();
//    }
//
//    private void initClientTableListener() {
//        clientTableView.setRowFactory(tv -> {
//            TableRow<Client> row = new TableRow<>();
//            row.setOnMouseClicked(event -> {
//                if (event.getClickCount() == 2 && (!row.isEmpty())) {
//                    fillAllFields(row.getItem());
//                }
//            });
//            return row;
//        });
//    }
//
//    private void initClientTable() {
//        clientTableView = new TableView<>(FXCollections.observableList(clientTableViewContents));
//        clientTableView.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
//
//        TableColumn id = new TableColumn("ID");
//        id.setCellValueFactory(new PropertyValueFactory("id"));
//
//        TableColumn name = new TableColumn("NAME");
//        name.setCellValueFactory(new PropertyValueFactory("name"));
//
//        TableColumn nif = new TableColumn("NIF");
//        nif.setCellValueFactory(new PropertyValueFactory("nif"));
//
//        TableColumn dob = new TableColumn("DOB");
//        dob.setCellValueFactory(new PropertyValueFactory("dob"));
//
//        TableColumn phone = new TableColumn("PHONE");
//        phone.setCellValueFactory(new PropertyValueFactory("telephone"));
//
//        TableColumn mobile = new TableColumn("MOBILE");
//        mobile.setCellValueFactory(new PropertyValueFactory("mobile"));
//
//        TableColumn email = new TableColumn("EMAIL");
//        email.setCellValueFactory(new PropertyValueFactory("email"));
//
//        TableColumn occupation = new TableColumn("OCCUPATION");
//        occupation.setCellValueFactory(new PropertyValueFactory("occupation"));
//
//        clientTableView.getColumns().addAll(id, name, nif, dob, phone, mobile, email, occupation);
//        dataContainer.getChildren().add(clientTableView);
//    }
//
//    private void loadClientTable() {
//        Task<ObservableList<Client>> task = new Task<ObservableList<Client>>() {
//            @Override
//            protected ObservableList<Client> call() throws Exception {
//                return FXCollections.observableArrayList(clientTableViewContents.stream().collect(Collectors.toList()));
//            }
//        };
//
//        task.setOnSucceeded(event -> {
//            data = task.getValue();
//            clientTableView.setItems(FXCollections.observableList(data));
//        });
//
//        Thread th = new Thread(task);
//        th.setDaemon(true);
//        th.start();
//    }
//
//    private void updateClientListView() throws Exception {
//        clientTableViewContents = service.getAll();
//        loadClientTable();
//    }
//
//    private void clearAllFields() {
//        idField.setText(null);
//        nameField.setText(null);
//        nifField.setText(null);
//        dobField.setValue(null);
//        phoneField.setText(null);
//        mobileField.setText(null);
//        emailField.setText(null);
//        occupationField.setText(null);
//        passwordField.setText(null);
//    }
//
//    private void fillAllFields(Client client) {
//        idField.setText(client.getId().toString());
//        nameField.setText(client.getName());
//        nifField.setText(client.getNif());
//        dobField.setValue(client.getDob());
//        phoneField.setText(client.getTelephone());
//        mobileField.setText(client.getMobile());
//        emailField.setText(client.getEmail());
//        occupationField.setText(client.getOccupation());
//        passwordField.setText(client.getPassword());
//    }
//
//    private void showError(Exception e) {
//        Alert errorAlert = new Alert(AlertType.ERROR);
//        errorAlert.setHeaderText("Oh no!");
//        errorAlert.setContentText(e.getMessage());
//        errorAlert.showAndWait();
//    }
//
//}