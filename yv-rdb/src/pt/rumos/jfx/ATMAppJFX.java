//package pt.rumos.jfx;
//
//import java.io.IOException;
//
//import org.graalvm.compiler.phases.common.NodeCounterPhase.Stage;
//
//import javafx.application.Application;
//import javafx.fxml.FXMLLoader;
//import javafx.scene.Parent;
//import javafx.scene.Scene;
//import jdk.tools.jlink.internal.Platform;
//
//public class ATMAppJFX extends Application {
//
//    @Override
//    public void start(Stage stage) {
//        try {
//            Thread.currentThread().setUncaughtExceptionHandler((thread, throwable) -> {
//                throwable.printStackTrace();
//            });
//
//            Parent root = FXMLLoader.load(getClass().getResource("App.fxml"));
//
//            Scene scene = new Scene(root, 1024, 768);
//            scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
//            stage.setResizable(false);
//            stage.setScene(scene);
//            stage.setTitle("RDB ATM module");
//            stage.show();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }
//
//    public void run() {
//        launch();
//    }
//
//    @Override
//    public void stop() {
//        Platform.exit();
//    }
//
//}