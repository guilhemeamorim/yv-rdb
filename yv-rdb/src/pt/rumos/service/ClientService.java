package pt.rumos.service;

import java.util.List;

import pt.rumos.exception.ResourceNotFoundException;
import pt.rumos.model.Client;

public interface ClientService {

    Client save(Client client);

    Client update(Client client);

    List<Client> getAll();

    Client getById(Long id);

    Client getByNif(String nif) throws ResourceNotFoundException;

    void deleteById(Long id);

}
