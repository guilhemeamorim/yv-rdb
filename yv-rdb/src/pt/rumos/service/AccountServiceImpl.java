package pt.rumos.service;

import java.util.List;

import pt.rumos.model.Account;
import pt.rumos.repository.AccountRepository;

public class AccountServiceImpl implements AccountService {

    private AccountRepository accountRepository;

    public AccountServiceImpl(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @Override
    public Account save(Account account) {
        return accountRepository.save(account).get();
    }

    @Override
    public Account update(Account account) {
        return accountRepository.update(account).get();
    }

    @Override
    public List<Account> getAll() {
        return accountRepository.findAll();
    }

    @Override
    public Account getById(Long id) {
        return accountRepository.findById(id).get();
    }

    @Override
    public void deleteById(Long id) {
        accountRepository.deleteById(id);
    }

}
