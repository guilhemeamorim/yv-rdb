package pt.rumos.service;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import pt.rumos.exception.ResourceNotFoundException;
import pt.rumos.model.Client;
import pt.rumos.repository.ClientRepository;

public class ClientServiceImpl implements ClientService {

    private ClientRepository clientRepository;

    public ClientServiceImpl(ClientRepository clientRepository) {
        this.clientRepository = clientRepository;
    }

    @Override
    public Client save(Client client) {
        boolean isUnderage = isUnderage(client);

        Optional<Client> savedClient = clientRepository.save(client);
        if (savedClient.isPresent()) {
            // Saved
            return savedClient.get();
        } else {
            // Not Saved
            throw new RuntimeException("Client was not saved");
        }

    }

    private boolean isUnderage(Client client) {
        LocalDate dateOfBirth = client.getDob();
        LocalDate currentTime = LocalDate.now();
        Long age = ChronoUnit.YEARS.between(dateOfBirth, currentTime);
        return age < 18;
    }

    @Override
    public Client update(Client client) {
        // Must perform 'isPresent' check...
        return clientRepository.update(client).get();
    }

    @Override
    public List<Client> getAll() {
        List<Client> realClients = new ArrayList<Client>();
        List<Client> clients = clientRepository.findAll();
        for (int i = 0; i < clients.size(); i++) {
            if (clients.get(i) != null) {
                realClients.add(clients.get(i));
            }
        }
        return realClients;
    }

    @Override
    public Client getById(Long id) {
        // Must perform 'isPresent' check...
        return clientRepository.findById(id).get();
    }

    @Override
    public Client getByNif(String nif) throws ResourceNotFoundException {
        Optional<Client> existingClient = clientRepository.findByNif(nif);

        if (existingClient.isPresent()) {
            return existingClient.get();
        }

        throw new ResourceNotFoundException("Client with NIF: " + nif + " not found.");
    }

    @Override
    public void deleteById(Long id) {
        Optional<Client> existingClient = clientRepository.findById(id);
        if (existingClient.isPresent()) {
            clientRepository.deleteById(id);
        }
    }
}