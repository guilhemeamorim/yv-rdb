package pt.rumos.service;

import java.util.List;

import pt.rumos.model.Account;

public interface AccountService {
    Account save(Account account);

    Account update(Account account);

    List<Account> getAll();

    Account getById(Long id);

    void deleteById(Long id);
}
