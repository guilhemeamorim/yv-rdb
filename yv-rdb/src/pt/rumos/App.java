package pt.rumos;

import java.util.Scanner;

import pt.rumos.console.ATMAppConsole;
import pt.rumos.console.ManagementApp;

public class App {

    // Única classe com MAIN <--
    // Ponto de início do código
    public static void main(String[] args) {
        @SuppressWarnings("resource")
        Scanner scanner = new Scanner(System.in);
        ManagementApp managementApp = new ManagementApp();
        ATMAppConsole atmAppConsole = new ATMAppConsole();
        // ATMAppJFX atmAppJFX = new ATMAppJFX();
        int option = 0;
        do {
            System.out.println("Select an Option:");
            System.out.println("0: Exit");
            System.out.println("1: Management App");
            System.out.println("2: ATM App - CONSOLE");
            // System.out.println("3: ATM App - JFX");
            option = scanner.nextInt();
            switch (option) {
                case 1:
                    managementApp.run();
                    break;
                case 2:
                    atmAppConsole.run();
                    break;
                // case 3:
                // atmAppJFX.run();
                // break;
            }

        } while (option != 0);
        System.out.println("Thanks for using RDB");
    }
}