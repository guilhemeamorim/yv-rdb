package pt.rumos.repository;

import java.util.List;
import java.util.Optional;

import pt.rumos.model.Account;

public interface AccountRepository {
    Optional<Account> save(Account account);

    Optional<Account> update(Account account);

    List<Account> findAll();

    Optional<Account> findById(Long id);

    void deleteById(Long id);
}
