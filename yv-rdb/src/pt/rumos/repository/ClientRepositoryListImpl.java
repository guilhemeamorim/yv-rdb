package pt.rumos.repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import pt.rumos.model.Client;

public class ClientRepositoryListImpl implements ClientRepository {

    private List<Client> clients = new ArrayList<Client>();
    private static long id = 0;

    @Override
    public Optional<Client> save(Client client) {
        client.setId(id);
        clients.add(client);
        id++;
        return Optional.of(client);
    }

    @Override
    public Optional<Client> update(Client client) {
        Long clientId = client.getId();
        for (int i = 0; i < clients.size(); i++) {
            Client c = clients.get(i);
            Long currentClientId = c.getId();
            if (currentClientId.equals(clientId)) {
                clients.set(i, client);
            }
        }
        return Optional.of(client);
    }

    @Override
    public List<Client> findAll() {
        return clients;
    }

    @Override
    public Optional<Client> findById(Long id) {
        for (int i = 0; i < clients.size(); i++) {
            Client c = clients.get(i);
            Long currentClientId = c.getId();
            if (currentClientId.equals(id)) {
                return Optional.of(c);
            }
        }
        return Optional.empty();
    }
    
    @Override
    public Optional<Client> findByNif(String nif) {
        for (int i = 0; i < clients.size(); i++) {
            Client c = clients.get(i);
            String currentClientNif = c.getNif();
            if (currentClientNif.equals(nif)) {
                return Optional.of(c);
            }
        }
        return Optional.empty();
    }

    @Override
    public void deleteById(Long id) {
        List<Client> newClients = new ArrayList<Client>();
        
        for (int i = 0; i < clients.size(); i++) {
            Client c = clients.get(i);
            Long currentClientId = c.getId();
            if (!currentClientId.equals(id)) {
                newClients.add(c);
            }
        }
        
        clients = newClients;

    }

}
