package pt.rumos.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import pt.rumos.database.Database;
import pt.rumos.model.Client;

public class ClientRepositorySQLImpl implements ClientRepository {

    @Override
    public Optional<Client> save(Client client) {
        String sql = "INSERT INTO client (name, nif, password, dob, phone, mobile, email, occupation) "
                + "VALUES ("
                + "'" + client.getName()         + "',"
                + "'" + client.getNif()          + "',"
                + "'" + client.getPassword()     + "',"
                + "'" + client.getDob()          + "',"
                + "'" + client.getTelephone()    + "',"
                + "'" + client.getMobile()       + "',"
                + "'" + client.getEmail()        + "',"
                + "'" + client.getOccupation()   + "');";
        Database.execute(sql);
        return findByNif(client.getNif());
    }

    @Override
    public Optional<Client> update(Client client) {
        String sql = "UPDATE client SET "
                + "name        = '" + client.getName()       + "',"
                + "nif         = '" + client.getNif()        + "',"
                + "password    = '" + client.getPassword()   + "',"
                + "dob         = '" + client.getDob()        + "',"
                + "phone       = '" + client.getTelephone()  + "',"
                + "mobile      = '" + client.getMobile()     + "',"
                + "email       = '" + client.getEmail()      + "',"
                + "occupation  = '" + client.getOccupation() + "' "
                + "WHERE id    =  " + client.getId()         + ";";
       Database.execute(sql);
       return findById(client.getId());
    }

    @Override
    public List<Client> findAll() {
        String sql = "SELECT * FROM client;";
        ResultSet rs = Database.executeQuery(sql);
        return extractList(rs);
    }

    @Override
    public Optional<Client> findById(Long id) {
        String sql = "SELECT * FROM client WHERE id = " + id + ";";
        ResultSet rs = Database.executeQuery(sql);
        return extractObject(rs);
    }

    @Override
    public Optional<Client> findByNif(String nif) {
        String sql = "SELECT * FROM client WHERE nif LIKE '" + nif + "';";
        ResultSet rs = Database.executeQuery(sql);
        return extractObject(rs);
    }

    @Override
    public void deleteById(Long id) {
        String sql = "DELETE FROM client WHERE id = " + id + ";";
        Database.execute(sql);
    }

    private List<Client> extractList(ResultSet rs) {
        try {
            List<Client> clients = new ArrayList<Client>();
            while (rs.next()) {
                Client client = buildObject(rs);
                clients.add(client);
            }
            return clients;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Collections.emptyList();
    }

    private Optional<Client> extractObject(ResultSet rs) {
        try {
            if (rs.next()) {
                Client client = buildObject(rs);
                return Optional.of(client);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }

    private Client buildObject(ResultSet rs) throws SQLException {
        Client client = new Client();
        client.setId(rs.getLong(1));
        client.setName(rs.getString(2));
        client.setNif(rs.getString(3));
        client.setPassword(rs.getString(4));
        client.setDob(rs.getDate(5).toLocalDate());
        client.setTelephone(rs.getString(6));
        client.setMobile(rs.getString(7));
        client.setEmail(rs.getString(8));
        client.setOccupation(rs.getString(9));
        return client;
    }

}
