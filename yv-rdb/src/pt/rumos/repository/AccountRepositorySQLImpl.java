package pt.rumos.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import pt.rumos.database.Database;
import pt.rumos.model.Account;
import pt.rumos.model.Client;

public class AccountRepositorySQLImpl implements AccountRepository {

    @Override
    public Optional<Account> save(Account account) {
        return Optional.empty();
    }

    @Override
    public Optional<Account> update(Account account) {
        return Optional.empty();
    }

    @Override
    public List<Account> findAll() {
        return Collections.emptyList();
    }

    @Override
    public Optional<Account> findById(Long id) {
        String sql = "SELECT * FROM account WHERE id = " + id + ";";
        ResultSet rs = Database.executeQuery(sql);
        return extractObject(rs);
    }

    @Override
    public void deleteById(Long id) {

    }
    
    private Optional<Account> extractObject(ResultSet rs) {
        try {
            if (rs.next()) {
                Account account = buildObject(rs);
                return Optional.of(account);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }
    
    private Account buildObject(ResultSet rs) throws SQLException {
        Account account = new Account();
        account.setId(rs.getLong(1));
        account.setAccountNumber(rs.getInt(2));
        Long clientId = rs.getLong(3);
        account.setBalance(rs.getDouble(4));
        
        String sql = "SELECT * FROM client WHERE id = " + clientId + ";";
        rs = Database.executeQuery(sql);
        rs.next();
        Client c = new Client();
        c.setId(rs.getLong(1));
        c.setName(rs.getString(2));
        c.setNif(rs.getString(3));
        c.setPassword(rs.getString(4));
        c.setDob(rs.getDate(5).toLocalDate());
        c.setTelephone(rs.getString(6));
        c.setMobile(rs.getString(7));
        c.setEmail(rs.getString(8));
        c.setOccupation(rs.getString(9));
        account.setOwner(c);
        
        sql = "SELECT * FROM account_client, client WHERE id_account = " + account.getId() + " AND id_client = client.id;";
        rs = Database.executeQuery(sql);
        List<Client> secondaryOwners = new ArrayList<Client>();
        while (rs.next()) {
            Client secondary = new Client();
            secondary.setId(rs.getLong(3));
            secondary.setName(rs.getString(4));
            secondary.setNif(rs.getString(5));
            secondary.setPassword(rs.getString(6));
            secondary.setDob(rs.getDate(7).toLocalDate());
            secondary.setTelephone(rs.getString(8));
            secondary.setMobile(rs.getString(9));
            secondary.setEmail(rs.getString(10));
            secondary.setOccupation(rs.getString(11));
            secondaryOwners.add(secondary);
        }
        account.setSecondaryOwners(secondaryOwners);
        return account;
    }

}
