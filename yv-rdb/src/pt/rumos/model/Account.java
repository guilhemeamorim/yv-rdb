package pt.rumos.model;

import java.util.List;

public class Account {

    private Long id;
    private Integer accountNumber;
    private Client owner;
    private List<Client> secondaryOwners;
    private Double balance;
    
    public Long getId() { return id; }
    public void setId(Long id) { this.id = id; }
    
    public Integer getAccountNumber() { return accountNumber; }
    public void setAccountNumber(Integer accountNumber) { this.accountNumber = accountNumber; }
    
    public Client getOwner() { return owner; }
    public void setOwner(Client owner) { this.owner = owner; }
    
    public List<Client> getSecondaryOwners() { return secondaryOwners; }
    public void setSecondaryOwners(List<Client> secondaryOwners) { this.secondaryOwners = secondaryOwners; }
    
    public Double getBalance() { return balance; }
    public void setBalance(Double balance) { this.balance = balance; }
    
    @Override
    public String toString() {
        return "Account [id=" + id + ", accountNumber=" + accountNumber + ", owner=" + owner + ", secondaryOwners=" + secondaryOwners
                + ", balance=" + balance + "]";
    }
}