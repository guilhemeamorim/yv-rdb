package pt.rumos;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import pt.rumos.model.Account;
import pt.rumos.model.Client;

public class DatabaseConnection {

    public static void main(String[] args) throws SQLException {
        // Connection String - Read vendor instructions.
        String url = "jdbc:mysql://localhost:3306/rdb-marques";
        String username = "root";
        String password = "root";

        String query = "SELECT id AS ACC_ID, account_number as ACC_NUM, balance as BAL, id_client as OWNER FROM account;";

        Connection conn = DriverManager.getConnection(url, username, password);
        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery(query);
        while (rs.next()) {
            Account acc = new Account();
            acc.setId(rs.getLong("ACC_ID"));
            acc.setAccountNumber(rs.getInt("ACC_NUM"));
            acc.setBalance(rs.getDouble("BAL"));

            query = "SELECT * FROM client WHERE id = " + rs.getLong("OWNER") + ";";
            rs = stmt.executeQuery(query);
            rs.next();
            Client c = new Client();
            c.setId(rs.getLong(1));
            c.setName(rs.getString(2));
            c.setNif(rs.getString(3));
            c.setPassword(rs.getString(4));
            c.setDob(rs.getDate(5).toLocalDate());
            c.setTelephone(rs.getString(6));
            c.setMobile(rs.getString(7));
            c.setEmail(rs.getString(8));
            c.setOccupation(rs.getString(9));
            acc.setOwner(c);
            System.out.println(acc);
        }
    }

    // public static void main(String[] args) throws SQLException {
    // // Connection String - Read vendor instructions.
    // String url = "jdbc:mysql://localhost:3306/rdb-marques";
    // String username = "root";
    // String password = "root";
    //
    // String query = "SELECT * FROM client;";
    //
    // Connection conn = DriverManager.getConnection(url, username, password);
    // Statement stmt = conn.createStatement();
    // ResultSet rs = stmt.executeQuery(query);
    // List<Client> clients = new ArrayList<Client>();
    // while (rs.next()) {
    // Client c = new Client();
    // c.setId(rs.getLong(1));
    // c.setName(rs.getString(2));
    // c.setNif(rs.getString(3));
    // c.setPassword(rs.getString(4));
    // c.setDob(rs.getDate(5).toLocalDate());
    // c.setTelephone(rs.getString(6));
    // c.setMobile(rs.getString(7));
    // c.setEmail(rs.getString(8));
    // c.setOccupation(rs.getString(9));
    // clients.add(c);
    // }
    // for (Client client : clients) {
    // System.out.println(client);
    // }
    // }

    // public static void main(String[] args) throws SQLException {
    // // Connection String - Read vendor instructions.
    // String url = "jdbc:mysql://localhost:3306/rdb-marques";
    // String username = "root";
    // String password = "root";
    //
    // Client client = new Client();
    // client.setName("Dr. Caturra");
    // client.setEmail("dr.caturra@gmail.com");
    //
    // String sql =
    // "INSERT INTO client ("
    // + "name, "
    // + "nif, "
    // + "password, "
    // + "dob, "
    // + "phone, "
    // + "mobile, "
    // + "email, "
    // + "occupation) "
    // + "VALUES ("
    // + "'" + client.getName() + "', "
    // + "'" + client.getNif() + "', "
    // + "'" + client.getPassword() + "', "
    // + "'1986-12-04', " // ?
    // + "'" + client.getTelephone() + "', "
    // + "'" + client.getMobile() + "', "
    // + "'" + client.getEmail() + "', "
    // + "'" + client.getOccupation() + "' );";
    //
    // System.out.println(sql);
    //
    // Connection conn = DriverManager.getConnection(url, username, password);
    // Statement stmt = conn.createStatement();
    // boolean executed = stmt.execute(sql);
    //
    // System.out.println(executed);
    // }
}
