package pt.rumos.service;

import java.time.LocalDate;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import pt.rumos.exception.ResourceNotFoundException;
import pt.rumos.model.Client;
import pt.rumos.repository.ClientRepositoryArrayImpl;

public class ClientServiceImplTest {
    private static ClientService clientService;

    // private static ClientService clientService = new ClientServiceImpl(new ClientRepositoryArrayImpl());

    // clientService = clientServiceImpl
    // clientServiceImpl = ClientRepositoryArrayImpl
    // ClientRepositoryArrayImpl = Client[3]

    @Before
    public void init() {
        clientService = new ClientServiceImpl(new ClientRepositoryArrayImpl());
    }

    // @Test
    // public void save_isIdFour() {
    // clientService.save(new Client()); //0
    // clientService.deleteById(0L);
    // clientService.save(new Client()); //1
    // clientService.deleteById(1L);
    // clientService.save(new Client()); //2
    // clientService.save(new Client()); //3
    // Client savedClient = clientService.save(new Client()); //4
    // Assert.assertEquals(savedClient.getId(), Long.valueOf(4));
    // }

    @Test
    public void save_isNotNull() {
        Client client = new Client();
        client.setDob(LocalDate.now());
        Client savedClient = clientService.save(client);
        Assert.assertNotNull(savedClient);
    }

    @Test
    public void save_isIdZero() {
        Client client = new Client();
        client.setDob(LocalDate.now());
        Client savedClient = clientService.save(client);
        Assert.assertEquals(Long.valueOf(0), savedClient.getId());
    }

    @Test
    public void getAll_hasNoNulls() {
        List<Client> clients = clientService.getAll();
        Assert.assertTrue(clients.isEmpty());
    }

    // @Test
    // public void deleteById_idExists() {
    // // Arrange
    // Client client = new Client();
    // Client savedClient = clientService.save(client);
    // Long existingId = savedClient.getId();
    //
    // // Act
    // clientService.deleteById(existingId);
    //
    // // Assert
    //
    // }

    @Test
    public void deleteById_idDoesntExist() {
        // Arrange
        String errorMessage = "Client not found";
        Long nonExistingId = -1L;

        // Act + Assert
        // @formatter:off
        ResourceNotFoundException exception = Assert.assertThrows(
                    ResourceNotFoundException.class,
                    () -> clientService.deleteById(nonExistingId)
                );
        // @formatter:on
        Assert.assertNotNull(exception);
        Assert.assertEquals(errorMessage, exception.getMessage());

    }

}
