package pt.rumos;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import pt.rumos.model.Pessoa;

public class PreProjectApp {

//    public static void main(String[] args) {
//        String nifString = "000000001";
//        Integer nif = Integer.valueOf(nifString);
//        System.out.println(nif);
//    }

public static void main(String[] args) {

    Scanner scanner = new Scanner(System.in);
    List<Pessoa> pessoas = new ArrayList<Pessoa>();
    int option = 0;
    do {
        System.out.println("O que você quer fazer ?");
        System.out.println("0: sair");
        System.out.println("1: adicionar pessoa");
        System.out.println("2: listar todos as pessoas");
        option = scanner.nextInt();
        switch (option) {
            case 1:
                System.out.println("Diga-me seu nome:");
                String nome = scanner.next();
                System.out.println("Diga-me a sua idade:");
                Integer idade = scanner.nextInt();
                Pessoa p = new Pessoa();
                p.setNome(nome);
                p.setIdade(idade);
                pessoas.add(p);
                break;
            case 2:
                for (Pessoa pessoa : pessoas) {
                    System.out.println(pessoa);
                }
                break;
        }

    } while (option != 0);
    System.out.println("Obrigado por usar o armazenador de pessoas");
}

}
