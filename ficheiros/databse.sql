/*
https://www.w3schools.com/sql/default.asp
DML (Data Modification Language)
DDL (Data Definition Language)

UUID - Universal Unique Identifier

C - Create
	INSERT
R - Retrieve
	SELECT
U - Update
	UPDATE
D - Delete
	DELETE
*/

SELECT * FROM client;

INSERT INTO client (name) VALUES ('Yuri');

INSERT INTO client (name,          nif,        password,   dob,          phone, mobile,              email,                 occupation)
            VALUES ('Yuri Valle', '289582580', '********', '1986-12-04', '',    '+352 661 89 17 21', 'yuri.valle@gmail.com', 'Teacher');

UPDATE client SET name = 'John Doe';

UPDATE client SET name = 'John Doe' WHERE id = 22;

SELECT * FROM client WHERE id = 21; /* for numerical values */
SELECT * FROM client WHERE nif LIKE '289582580'; /* for string values */
UPDATE client SET name = 'John Doe Updated' WHERE nif LIKE '%456%'; /* for string values with wildcard */

DELETE FROM client WHERE name LIKE '%Doe%';

SELECT id AS IDENTIFIER, name AS NOME FROM client;

SELECT acc.account_number AS 'ACCOUNT NUMBER', acc.balance, cli.name FROM account AS acc, client AS cli WHERE acc.account_number = 123 AND acc.id_client = cli.id;

INSERT INTO account (account_number, id_client, balance) VALUES (123, 20, 50.00);