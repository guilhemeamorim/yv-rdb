package pt.rumos;

import java.util.Scanner;

public class App {
    private static int a = 0;
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        String name = "Yuri";
//        
//        char myChar = 'a'; //16 bits
//        
//        final String MY_A = "a"; //+ de 16 bits
//        
//        MY_A = "ABCD";
//        
//        boolean y = true;
//        
//        byte variable = (byte) y;
        
        final float PI;
        PI = 3.1415926F;
        // PI = 1F;

        final double PI_FINAL = Math.PI;

        System.out.println("VALOR ATUAL:" + name);

        System.out.print("Digite o seu nome:");

        name = scanner.nextLine();

        System.out.println("Seja bemvindo(a): " + name);
        
    }
}