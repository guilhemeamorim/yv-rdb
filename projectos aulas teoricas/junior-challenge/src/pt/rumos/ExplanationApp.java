package pt.rumos;

import java.util.Arrays;

public class ExplanationApp {

    public static void main(String[] args) {
        int[] arrayDeInteiros = new int[] { 8, 2, 1, 7, 9, 9 };
        // Quando só temos um array ele fica como na linha 8, na horizontal.
        // Entretanto, a mesma 'visualização' no array de 2 dimensões é diferente.
        // Para 'ver' o array temos que o imprimir por linha, neste caso, ele fica como no literal.
        // A confusão ocorreu porque não ficou explícito no exercício quais eram os dados de cada linha.
        // Minha intuição me levou a fazer como no exercício.
        // Porém, no slides aprendemos a visualizar diferente .
        //@formatter:off
        int[][] arrayDeArrayDeInteiros = new int[][]
            {
                { 8, 0, 0, 0, 0, 0 },
                { 2, 0, 0, 0, 0, 0 },
                { 1, 0, 0, 0, 0, 0 },
                { 7, 0, 0, 0, 0, 0 },
                { 9, 0, 0, 0, 0, 0 },
                { 9, 0, 0, 0, 0, 0 },
            };
        //@formatter:on
        System.out.println("Array de inteiros");
        System.out.println(Arrays.toString(arrayDeInteiros));
        System.out.println("\nImpresso por linha");
        for (int[] is : arrayDeArrayDeInteiros) {
            System.out.println(Arrays.toString(is));
        }
        System.out.println("\nImpresso por coluna");
        for (int linha = 0; linha < arrayDeArrayDeInteiros.length; linha++) {
            for (int coluna = 0; coluna < arrayDeArrayDeInteiros.length; coluna++) {
                System.out.print(arrayDeArrayDeInteiros[coluna][linha]+" ");
            }
            System.out.print("\n");
        }
    }
}
