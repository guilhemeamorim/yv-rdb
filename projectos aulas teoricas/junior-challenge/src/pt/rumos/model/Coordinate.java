package pt.rumos.model;

public class Coordinate {
    private Integer x;
    private Integer y;
    private Integer value;

    public Coordinate(Integer x, Integer y, Integer value) {
        if (x == null || x == null || value == null) {
            throw new RuntimeException("X, Y and values must be present.");
        }
        this.x = x;
        this.y = y;
        this.value = value;
    }

    public Integer getX() { return x; }
    public Integer getY() { return y; }
    public Integer getValue() { return value; }
}
