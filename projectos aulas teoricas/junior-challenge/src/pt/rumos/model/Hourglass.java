package pt.rumos.model;

import java.util.List;

public class Hourglass {
    // Expected 'Shape' within a 2D Matrix

    // X X X
    //   X
    // X X X

    private Integer sum;
    private List<Coordinate> coordinates;

    public Hourglass(List<Coordinate> coordinates) {
        if (coordinates == null) {
            throw new RuntimeException("Coordinates can't be null.");
        }
        if (coordinates.size() != 7) {
            throw new RuntimeException("There must be exactly 7 coordinates.");
        }
        this.coordinates = coordinates;
        for (Coordinate coordinate : coordinates) {
            this.sum = this.sum + coordinate.getValue();
        }
    }

    public Integer getSum() { return sum; }
    public List<Coordinate> getCoordinates() { return coordinates; }
}