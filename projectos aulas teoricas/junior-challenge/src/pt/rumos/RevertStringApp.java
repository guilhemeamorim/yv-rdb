package pt.rumos;

public class RevertStringApp {

    public static void main(String[] args) {
        String str = null;
        String value = revertString(str);
        System.out.println(str);
        System.out.println(value);
    }

    private static String revertString(String str) {
        char[] response = new char[str.length()];
        System.out.println("Tamanho da String: " + str.length());
        String returnValue = "";
        for (int i = str.length() - 1; i >= 0; i--) {
            // System.out.println("Posição da string: " + i);
            // System.out.println("Posição da resposta: " + (str.length() - (i + 1)));
            response[(str.length() - (i + 1))] = str.charAt(i);
            returnValue = returnValue + str.charAt(i);
        }

        return returnValue;
    }

    private static String revertString1(String str) {
        StringBuilder strBuilder = new StringBuilder();

        for (int i = str.length() - 1; i >= 0; i--) {
            strBuilder.append(str.charAt(i));
        }

        return strBuilder.toString();
    }

}
