package pt.rumos;

import java.util.ArrayList;
import java.util.List;

public class HourglassApp {
    public static void main(String[] args) {
        MatrixManager matrixManager = new MatrixManager();

        int matrix[][] = new int[6][6];
//        List<List<Integer>> matrix = new ArrayList<>();

        Boolean isRandom = true;
        matrixManager.populate(isRandom, matrix);
        matrixManager.print(matrix);

        int greatestSum = Integer.MIN_VALUE;

        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                int hourglassSum = getHourglassSum(i, j, matrix);
                if (greatestSum < hourglassSum) {
                    greatestSum = hourglassSum;
                };
            }
        }
        System.out.println("The greatest sum is: " + greatestSum);
    }


    private static int getHourglassSum(int i, int j, int[][] matrix) {
        int hourglassSum = 0;
        hourglassSum = hourglassSum + matrix[i][j];
        hourglassSum = hourglassSum + matrix[i + 1][j];
        hourglassSum = hourglassSum + matrix[i + 2][j];
        hourglassSum = hourglassSum + matrix[i + 1][j + 1];
        hourglassSum = hourglassSum + matrix[i][j + 2];
        hourglassSum = hourglassSum + matrix[i + 1][j + 2];
        hourglassSum = hourglassSum + matrix[i + 2][j + 2];
        return hourglassSum;
    }

    private static int getHourglassSum(int i, int j, List<List<Integer>> matrix) {
        int hourglassSum = 0;
        hourglassSum = hourglassSum + matrix.get(i).get(j);
        hourglassSum = hourglassSum + matrix.get(i).get(j + 1);
        hourglassSum = hourglassSum + matrix.get(i).get(j + 2);
        hourglassSum = hourglassSum + matrix.get(i + 1).get(j + 1);
        hourglassSum = hourglassSum + matrix.get(i + 2).get(j);
        hourglassSum = hourglassSum + matrix.get(i + 2).get(j + 1);
        hourglassSum = hourglassSum + matrix.get(i + 2).get(j + 2);
        return hourglassSum;
    }
}