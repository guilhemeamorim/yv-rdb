package pt.rumos;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MatrixPopulator {
    
    public void populate(Boolean isRandom, List<List<Integer>> matrix) {
        if (isRandom) {
            for (int i = 0; i < 6; i++) {
                List<Integer> line = new ArrayList<Integer>();
                for (int j = 0; j < 6; j++) {
                    line.add(randomNumber(-9, 9));
                }
                matrix.add(line);
            }
            return;
        }
        matrix.add(new ArrayList<Integer>(Arrays.asList(1, 1, 1, 0, 0, 0)));
        matrix.add(new ArrayList<Integer>(Arrays.asList(0, 1, 0, 0, 0, 0)));
        matrix.add(new ArrayList<Integer>(Arrays.asList(1, 1, 1, 0, 0, 0)));
        matrix.add(new ArrayList<Integer>(Arrays.asList(0, 0, 2, 4, 4, 0)));
        matrix.add(new ArrayList<Integer>(Arrays.asList(0, 0, 0, 2, 0, 0)));
        matrix.add(new ArrayList<Integer>(Arrays.asList(0, 0, 1, 2, 4, 0)));
    }

    public void populate(Boolean isRandom, int[][] matrix) {
        if (isRandom) {
            for (int i = 0; i < matrix.length; i++) {
                for (int j = 0; j < matrix.length; j++) {
                    matrix[i][j] = randomNumber(-9, 9);
                }
            }
            return;
        }

        matrix[0][0] = 1;
        matrix[1][0] = 1;
        matrix[2][0] = 1;

        matrix[1][1] = 1;

        matrix[0][2] = 1;
        matrix[1][2] = 1;
        matrix[2][2] = 1;
        //
        matrix[2][3] = 2;
        matrix[3][3] = 4;
        matrix[4][3] = 4;

        matrix[3][4] = 2;

        matrix[2][5] = 1;
        matrix[3][5] = 2;
        matrix[4][5] = 4;
    }

    private int randomNumber(int min, int max) {
        return (int) ((Math.random() * (max - min)) + min);
    }

}
