package pt.rumos;

import java.util.List;

public class MatrixManager {
    private MatrixPopulator matrixPopulator = new MatrixPopulator();
    private MatrixPrinter matrixPrinter = new MatrixPrinter();

    public void populate(Boolean isRandom, int[][] matrix) {
        matrixPopulator.populate(isRandom, matrix);
    }

    public void populate(Boolean isRandom, List<List<Integer>> matrix) {
        matrixPopulator.populate(isRandom, matrix);
    }

    public void print(int[][] matrix) {
        matrixPrinter.print(matrix);
    }

    public void print(List<List<Integer>> matrix) {
        matrixPrinter.print(matrix);
    }
}
