package pt.rumos;

import java.util.List;

public class MatrixPrinter {
    public void print(int[][] matrix) {
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix.length; j++) {
                System.out.print(matrix[j][i] + " ");
            }
            System.out.print("\n");
        }
    }

    public void print(List<List<Integer>> matrix) {
        for (List<Integer> line : matrix) {
            System.out.println(line);
        }
    }
}
