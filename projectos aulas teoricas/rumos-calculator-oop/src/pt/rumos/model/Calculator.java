package pt.rumos.model;

public class Calculator {

    public Double sum(Double firstValue, Double secondValue) {
        return firstValue + secondValue;
    }

    public Double subtract(Double firstValue, Double secondValue) {
        return firstValue - secondValue;
    }

    public Double multiply(Double firstValue, Double secondValue) {
        return firstValue * secondValue;
    }

    public Double divide(Double firstValue, Double secondValue) {
        return firstValue / secondValue;
    }

}
