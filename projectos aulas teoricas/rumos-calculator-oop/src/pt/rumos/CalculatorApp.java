package pt.rumos;

import java.util.Scanner;

import pt.rumos.model.Calculator;

public class CalculatorApp {
    public static void main(String[] args) {
        Calculator calculator = new Calculator();
        Scanner scanner = new Scanner(System.in);

        System.out.println("Please input the first number");
        Double firstValue = scanner.nextDouble();

        System.out.println("Please input the second number");
        Double secondValue = scanner.nextDouble();

        System.out.println("Please select the operation: ");
        System.out.println("1: Sum");
        System.out.println("2: Subtract");
        System.out.println("3: Multiply");
        System.out.println("4: Divide");

        Integer option = scanner.nextInt();

        switch (option) {
            case 1:
                System.out.println(calculator.sum(firstValue, secondValue));
                break;
            case 2:
                System.out.println(calculator.subtract(firstValue, secondValue));
                break;
            case 3:
                System.out.println(calculator.multiply(firstValue, secondValue));
                break;
            case 4:
                System.out.println(calculator.divide(firstValue, secondValue));
                break;
            default:
                break;
        }

    }
}
