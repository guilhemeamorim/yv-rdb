package pt.rumos.modelos;

public class BocaDeFogao {

    private Double raio;
    private String tipoDeGeracaoDeCalor;

    public Double getRaio() {
        return raio;
    }

    public void setRaio(Double raio) {
        this.raio = raio;
    }

    public String getTipoDeGeracaoDeCalor() {
        return tipoDeGeracaoDeCalor;
    }

    public void setTipoDeGeracaoDeCalor(String tipoDeGeracaoDeCalor) {
        this.tipoDeGeracaoDeCalor = tipoDeGeracaoDeCalor;
    }


}