package pt.rumos.modelos;

public class Person {

    public String name;
    public int age;
    private int peso;
    private Sangue sangue;

//    public Person(Integer age) {
//        this.age = age;
//    }
//
//    public Person(String age) {
//        this.age = Integer.parseInt(age);
//    }

    // Método Construtor
    public Person(GrupoSanguineo grupoSanguineo, Integer peso) {
        this.peso = peso;
        this.sangue = new Sangue(grupoSanguineo, peso / 10);
    }

    public void talk() {
        System.out.println("Hi, my name is: " + name + ".");
    }

    public void sleep() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public int getAge() {
        return age;
    }


    public void setAge(int age) {
        this.age = age;
    }


    public int getPeso() {
        return peso;
    }


    public void setPeso(int peso) {
        this.peso = peso;
    }


    public Sangue getSangue() {
        return sangue;
    }

    public void setSangue(Sangue sangue) {
        if (sangue.getGrupo() == this.getSangue().getGrupo()) {
            this.sangue = sangue;
        }
    }
}