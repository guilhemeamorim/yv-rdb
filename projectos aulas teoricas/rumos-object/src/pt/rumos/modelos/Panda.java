package pt.rumos.modelos;

import java.util.Objects;

public class Panda {

    public String name;

    public void sayName() {
        System.out.println(this.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Panda other = (Panda) obj;
        return Objects.equals(name, other.name);
    }
}