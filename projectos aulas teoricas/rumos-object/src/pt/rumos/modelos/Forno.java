package pt.rumos.modelos;

public class Forno {

    private Boolean grill;
    private Integer potencia;

    public Boolean getGrill() {
        return grill;
    }

    public void setGrill(Boolean grill) {
        this.grill = grill;
    }

    public Integer getPotencia() {
        return potencia;
    }

    public void setPotencia(Integer potencia) {
        this.potencia = potencia;
    }


}
