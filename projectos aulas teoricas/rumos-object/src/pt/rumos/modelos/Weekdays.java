package pt.rumos.modelos;

public enum Weekdays {
    SEGUNDA("Segunda-Feira"),
    TERCA("Terça-Feira"),
    QUARTA("Quarta-Feira");

    private String value;

    Weekdays(String value) {
        this.value = value;
    }
    
    @Override
    public String toString() {
        return this.value;
    }
}