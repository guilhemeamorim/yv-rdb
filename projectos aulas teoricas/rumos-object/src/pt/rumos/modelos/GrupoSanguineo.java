package pt.rumos.modelos;

public enum GrupoSanguineo {
    A, B, AP, AN, BP, BN, OP, ON
}
