package pt.rumos.modelos;

import java.util.List;

public class Fogao extends Eletrodomestico {

    private List<BocaDeFogao> bocas;
    private Forno forno;

    private Integer altura;
    private Integer largura;
    private Integer profundidade;

    public Fogao(int preco) {
        super(preco);
    }

    public List<BocaDeFogao> getBocas() {
        return bocas;
    }

    public void setBocas(List<BocaDeFogao> bocas) {
        this.bocas = bocas;
    }

    public Forno getForno() {
        return forno;
    }

    public void setForno(Forno forno) {
        this.forno = forno;
    }

    public Integer getAltura() {
        return altura;
    }

    public void setAltura(Integer altura) {
        this.altura = altura;
    }

    public Integer getLargura() {
        return largura;
    }

    public void setLargura(Integer largura) {
        this.largura = largura;
    }

    public Integer getProfundidade() {
        return profundidade;
    }

    public void setProfundidade(Integer profundidade) {
        this.profundidade = profundidade;
    }


}