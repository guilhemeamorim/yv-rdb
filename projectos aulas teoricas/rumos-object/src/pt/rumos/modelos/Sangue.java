package pt.rumos.modelos;

public class Sangue {

    private GrupoSanguineo grupo;
    private Integer quantidade;

    public Sangue(GrupoSanguineo grupo, Integer quantidade) {
        this.grupo = grupo;
        this.quantidade = quantidade;
    }

    public GrupoSanguineo getGrupo() { return grupo; }
    
    public Integer getQuantidade() { return quantidade; }
    public void setQuantidade(Integer quantidade) { this.quantidade = quantidade; }


}