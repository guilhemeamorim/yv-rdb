package pt.rumos.modelos;

public class Car {
    private int wheels;
    private String color;
    private int serialNumber;

    public Car() {
        System.out.println("Empty constructor executed!");
    }

    public Car(int wheels) {
        this.wheels = wheels;
        System.out.println("Wheels constructor executed!");
    }

    public int getWheels() {
        return wheels;
    }

    public void setWheels(int wheels) {
        if (wheels <= 0) {
            System.out.println("Can't have <= 0 wheels");
            return;
        }
        this.wheels = wheels;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(int serialNumber) {
        this.serialNumber = serialNumber;
    }


    public class Bike {
        private String color;

        public String getColor() {
            return color;
        }

        public void setColor(String color) {
            this.color = color;
        }
    }

}