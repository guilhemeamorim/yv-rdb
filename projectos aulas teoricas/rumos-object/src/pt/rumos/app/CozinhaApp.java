package pt.rumos.app;

import pt.rumos.modelos.Eletrodomestico;
import pt.rumos.modelos.Fogao;
import pt.rumos.modelos.Frigorifico;

public class CozinhaApp {

    public static void main(String[] args) {
        Fogao fogao = new Fogao(5);
        Eletrodomestico frigorifico = new Frigorifico(1);

        fazAlgumaCoisa(fogao);
    }

    public static void fazAlgumaCoisa(Eletrodomestico e) {

    }

}