package pt.rumos.app;

import pt.rumos.modelos.GrupoSanguineo;
import pt.rumos.modelos.Person;
import pt.rumos.modelos.Sangue;
import pt.rumos.service.EnfermagemServiceImpl;

public class HospitalApp {
    public static void main(String[] args) {
        EnfermagemServiceImpl enfermaria = new EnfermagemServiceImpl();

        Person yuri = new Person(GrupoSanguineo.AP, 60);

        yuri.setName("Yuri");

        Sangue amostra = enfermaria.tirarSangue(yuri, 5);

        System.out.println(amostra.getGrupo());
        System.out.println(amostra.getQuantidade());

    }

}