package pt.rumos.app;

import java.util.ArrayList;
import java.util.List;

public class AppArrays {

    public static void main(String[] args) {

        List<Double> moedeiro = new ArrayList<Double>();

        moedeiro.add(12D); // 0
        moedeiro.add(12D); // 1
        moedeiro.add(18D); // 2
        moedeiro.add(20D); // 3
        moedeiro.add(12D); // 4
        moedeiro.add(12D); // 5
        moedeiro.add(12D); // 6

        int tamanho = moedeiro.size();

        Double uniqueIndex = moedeiro.get(8);

        // int age = 5;
        //
        // Double pocketMoney = 12.72D;

        // int[][][] moedeiro = new int[3][2][7];
        //
        // moedeiro[0][0] = 2;
        // moedeiro[1][0] = 1;
        // moedeiro[2][0] = 9;
        // moedeiro[3][0] = 32;
        // moedeiro[4][4] = 66;

        // int[][] array = new int[2][];
        // array[0] = new int[3];
        // array[1] = new int[2];


        // int[][] array = new int[][] {
        // new int[] {1, 2, 3},
        // new int[] {4, 5},
        // };

        // int[][] array = new int[2][]; // First dimension must always be assigned
        // array[0] = new int[3]; // Initializing second dimension
        // array[1] = new int[6];


        // int tmp1 = moedeiro[0];
        // int tmp2 = moedeiro[1];
        // int tmp3 = moedeiro[2];
        // int tmp4 = moedeiro[3];

        // moedeiro = new int[6];
        // moedeiro[0] = tmp1;
        // moedeiro[1] = tmp2;
        // moedeiro[2] = tmp3;
        // moedeiro[3] = tmp4;
        // moedeiro[4] = 11;
        // moedeiro[5] = 22;

        // System.out.println(Arrays.toString(moedeiro));

        // var array = { 1, 2, 3, 4, 5 };
        // quando for executar esta linha

        System.out.println(moedeiro);

        System.out.println(tamanho);

        System.out.println(uniqueIndex);

    }
}
