package pt.rumos.service;

import pt.rumos.modelos.Person;
import pt.rumos.modelos.Sangue;

public class EnfermagemServiceImpl implements EnfermagemService {

    /**
     * Verificar se é conteúdo de certificação! {@link Person}
     * 
     * @param utente
     * @param aRetirar
     * @return
     */
    @Override
    public Sangue tirarSangue(Person utente, Integer aRetirar) {
        int quantidadeDeSangueNoUtente = utente.getSangue().getQuantidade();
        if (quantidadeDeSangueNoUtente > aRetirar) {
            System.out.println("Estou a tirar sangue.");
            utente.getSangue().setQuantidade(quantidadeDeSangueNoUtente - aRetirar);
            return new Sangue(utente.getSangue().getGrupo(), aRetirar);
        } else {
            System.out.println("Não posso tirar sangue, não há o suficiente.");
            return null;
        }
    }

    @Override
    public void desinfectar() {
        System.out.println("Desinfectando!");
    }

    @Override
    public void vacinar() {

    }

}