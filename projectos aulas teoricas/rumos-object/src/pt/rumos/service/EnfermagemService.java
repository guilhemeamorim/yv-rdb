package pt.rumos.service;

import pt.rumos.modelos.Person;
import pt.rumos.modelos.Sangue;

public interface EnfermagemService {

    Sangue tirarSangue(Person utente, Integer quantidade);

    void desinfectar();

    void vacinar();

}