package pt.rumos.service;

import pt.rumos.modelos.Person;
import pt.rumos.modelos.Sangue;

public class EnfermagemServiceLowCostImpl implements EnfermagemService {

    @Override
    public Sangue tirarSangue(Person utente, Integer aRetirar) {
        int quantidadeDeSangueNoUtente = utente.getSangue().getQuantidade();
        if (quantidadeDeSangueNoUtente > aRetirar) {
            System.out.println("Estou a tirar sangue.");
            return new Sangue(utente.getSangue().getGrupo(), aRetirar);
        } else {
            System.out.println("Não posso tirar sangue, não há o suficiente.");
            return null;
        }
    }

    @Override
    public void desinfectar() {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void vacinar() {
        // TODO Auto-generated method stub
        
    }

}