package pt.rumos;

import java.util.Scanner;

import pt.rumos.service.CalculatorService;
import pt.rumos.service.CalculatorServiceImpl;
import pt.rumos.service.MenuService;
import pt.rumos.service.MenuServiceImpl;

public class CalculatorApp {
    public static void main(String[] args) {
        MenuService menu = new MenuServiceImpl();
        CalculatorService calculator = new CalculatorServiceImpl();
        Scanner scanner = new Scanner(System.in);

        System.out.println("Please input the first number");
        Double firstValue = scanner.nextDouble();

        System.out.println("Please input the second number");
        Double secondValue = scanner.nextDouble();

        menu.showCalculatorMenu();

        Integer option = scanner.nextInt();

        switch (option) {
            case 1:
                System.out.println(calculator.sum(firstValue, secondValue));
                break;
            case 2:
                System.out.println(calculator.subtract(firstValue, secondValue));
                break;
            case 3:
                System.out.println(calculator.multiply(firstValue, secondValue));
                break;
            case 4:
                System.out.println(calculator.divide(firstValue, secondValue));
                break;
            default:
                break;
        }


    }
}