package pt.rumos.service;

public class MenuServiceImpl implements MenuService {

    @Override
    public void showCalculatorMenu() {
        System.out.println("Please select the operation: ");
        System.out.println("1: Sum");
        System.out.println("2: Subtract");
        System.out.println("3: Multiply");
        System.out.println("4: Divide");
    }

}
