package pt.rumos.service;

public class CalculatorServiceImpl implements CalculatorService {

    @Override
    public Double sum(Double firstValue, Double secondValue) {
        return firstValue + secondValue;
    }

    @Override
    public Double subtract(Double firstValue, Double secondValue) {
        return firstValue - secondValue;
    }

    @Override
    public Double multiply(Double firstValue, Double secondValue) {
        return firstValue * secondValue;
    }

    @Override
    public Double divide(Double firstValue, Double secondValue) {
        return firstValue / secondValue;
    }

}
