package pt.rumos.service;

public interface CalculatorService {

    Double sum(Double firstValue, Double secondValue);

    Double subtract(Double firstValue, Double secondValue);

    Double multiply(Double firstValue, Double secondValue);

    Double divide(Double firstValue, Double secondValue);
}